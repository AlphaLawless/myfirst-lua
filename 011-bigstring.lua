quote = "I changed my password to incorrect. So that when I forget it, it always reminds me, your password is incorrect."

io.write("Quote Length : ", string.len(quote), "\n")

-- another option count length
io.write("Quote length : ", #quote, "\n")

io.write("Replace I with me ", string.gsub(quote, "I", "me"), "\n")

io.write("Index of password : ", string.find(quote, "password"), "\n")

io.write("Quote Upper : ", string.upper(quote), "\n")
io.write("Quote Lower : ", string.lower(quote), "\n")
