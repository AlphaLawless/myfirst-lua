-- Relational Operators: > < >= <= == ~= !=

age = 13

if age < 16 then
    io.write("You can go to drive school ", "\n")
    local localVar = 10 -- local variables and that just means that they are local
                        -- to this if statement and the value that is set here cannot
                        -- be found outside of the if statement.
elseif (age <= 16 ) and (age < 18) then
    io.write("You can drive ", "\n")
else
    io.write("you can vote", "\n")
end

print(localVar)
