-- Ternary Operators:

age = 18
-- canVote = age > 16 ? true : false

canVote = age > 16 and true or false

io.write("can I vote : ", tostring(canVote), "\n")
