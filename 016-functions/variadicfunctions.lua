function getSumMore(...)
    local sum = 0

    for k, v in pairs{...} do
        sum = sum + v
    end
    return sum
end

io.write("Sum ", getSumMore(1,2,3,4,5,6,7), "\n")
