-- co = coroutine.create(function ()
    -- print("hi")
-- end)

-- Fazer um load (de forma assincrona), demorar o processo de execução no resume(), p.s. da fazer usando yield()

-- print(coroutine.status(co))
--
-- print(coroutine.resume(co))
--
-- print(coroutine.status(co))
--
-- co1 = coroutine.create(function ()
    -- for i = 1, 5 do
        -- print("co", i)
        -- coroutine.yield()
    -- end
-- end)
--
-- print(coroutine.resume(co1))
-- print(coroutine.status(co1))
--
-- print(coroutine.resume(co1))
-- print(coroutine.status(co1))
--
-- print(coroutine.resume(co1))
-- print(coroutine.status(co1))
--
-- print(coroutine.resume(co1))
-- print(coroutine.status(co1))
--
-- print(coroutine.resume(co1))
-- print(coroutine.status(co1))
--
-- print(coroutine.resume(co1))
-- print(coroutine.status(co1))

co = coroutine.create(function()
    for i = 1, 10, 1 do
        print(i)
        print(coroutine.status(co))
        if i == 5 then
            coroutine.yield()
        end
    end
end)

print(coroutine.status(co))

coroutine.resume(co)

print(coroutine.status(co))

co2 = coroutine.create(function()
    for i = 101, 110, 1 do
        print(i)
    end
end)

coroutine.resume(co2)

coroutine.resume(co)

print(coroutine.status(co))
