function getSum(num1, num2)
    return num1 + num2
end

print(string.format("5 + 2 = %d", getSum(2,5)))

function splitStr(theString)
    stringTable = {}

    local i = 1

    for word in string.gmatch(theString, "[^%s]+") do
        stringTable[i] = word
        i = i + 1
    end

    return stringTable, i
end

splitStrTable, numOfStr = splitStr("The Turtle")

for j = 1, numOfStr - 1 do
    print(string.format("%d : %s", j, splitStrTable[j]))
end
