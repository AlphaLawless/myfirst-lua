doubleIt = function(x) return x * 2 end

print(doubleIt(4))

-- Function inside a function
function outerFunc()
    local i = 0

    return function()
        i = i + 1
        return i
    end
end

getI = outerFunc()

print(getI())
print(getI())
