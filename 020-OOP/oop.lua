Animal = {height = 0, weight = 0, name = "No Name", sound="No sound"}

function Animal:new (height, weight, name, sound)
    setmetatable({}, Animal)

    self.height = height
    self.weight = weight
    self.name = name
    self.sound = sound

    return self
end

function Animal:toString()
    catStr = string.format("%s weights %.1f lbs, is %.1f in tall and says %s", self.name, self.weight, self.height, self.sound)
    return catStr
end

spot = Animal:new(10, 15, "Spot", "Woof")

print(spot.weight)

print(spot:toString())

cat = Animal:new()

function cat:new (height, weight, name, sound, favFood)
    setmetatable({}, cat)

    self.height = height
    self.weight = weight
    self.name = name
    self.sound = sound
    self.favFood = favFood

    return self
end

function cat:toString()
    catStr = string.format("%s weights %.1f lbs, is %.1f in tall and says %s and loves %s", self.name, self.weight, self.height, self.sound, self.favFood)
    return catStr
end

fluffy = cat:new(10, 15, "Fluffy", "Meow", "Tuna")

print(fluffy:toString())


