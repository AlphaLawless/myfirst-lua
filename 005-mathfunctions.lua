-- Operators Math in Lua
io.write("5 + 3 = ", 5+5, "\n")
io.write("5 - 3 = ", 5-3, "\n")
io.write("5 x 3 = ", 5*3, "\n")
io.write("5 / 3 = ", 5/3, "\n")
io.write("5 % 3 = ", 5%3, "\n")

-- Composite Operators
-- number += 5
-- number -= 5
-- number *= 5
-- number /= 5

-- Math Functions: floor, ceil, max, min, sin, cos, tan,
-- asin, acos, exp, log, log10, pow, sqrt, random, randomseed

io.write("floor(2.345) : ", math.floor(2.345), "\n")
io.write("ceil(2.345) : ", math.ceil(2.345), "\n")
io.write("max(2, 3) : ", math.max(2, 3), "\n")
io.write("min(2, 3) : ", math.min(2, 3), "\n")
io.write("pow(8, 2) : ", math.pow(8, 3), "\n")
io.write("sqrt(64) : ", math.sqrt(64), "\n")
