name = "Alpha"

longString = [[
I am a very very long
string that goes on forever
]]
-- if you wanted to combine or concatenate the variables strings.
-- Use the operator .. for concatenate
longString = longString .. name

io.write(longString, "\n")

-- boolean

isAbleToDrive = true

io.write(type(isAbleToDrive), "\n")

io.write(type(makeUpVar), "\n")
-- The `makeUpVar` is a variable nil. It's not been defined with any
-- value
