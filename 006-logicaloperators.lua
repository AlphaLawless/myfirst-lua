-- Logical operators: and, or, not

age = 13
if (age < 14) or (age > 50) then
    io.write("You shouldn't work", "\n")
end
