num = 1
-- Blocks are denoted with keywords like do/end:
while num < 50 do
    print(string.format("Number = %d", num))
    num = num + 1
end
