num = 1
while num < 50 do
    num = num + 1    -- No ++ or += type operators
    print(string.format("Number = %d", num))
end
